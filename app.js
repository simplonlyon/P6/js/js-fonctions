"use strict"; // active le mode strict https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Strict_mode

// une fonction permet de factoriser le code ( faire en sorte de ne pas se répéter )
// une fonction est nommé lors de sa déclaration : function maFonction(){...}
// une fonction à une ou plusieurs entrée(s), appelées paramètres ou params
// une fonction peut retourner une valeur ( c'est sa sortie ) grâce à l'intruction `return`
// lorsque l'on appelle une fonction, celle-ci éxecute le code qui se trouve à l'intérieur des accolades ( aussi appelé un bloc de code ou simplement bloc )
// il est possible de récupérer la valeur de retour d'une fonction dans une variable de la façon suivante : let maVariable = maFonction();
// ce n'est pas toujours nécessaire de stocker la valeur retournée par une fonction dans une variable, on peux également l'utiliser directement : console.log(maFonction());
// ATTENTION !! il ne faut JAMAIS appeler une fonction sans mettre les parenthèses après son nom, même si elle ne prend pas de paramètres !


// la fonction suivante calcule les n premiers nombres de la suite de fibonacci ou n est au moins égal à 2 (https://fr.wikipedia.org/wiki/Suite_de_Fibonacci).
// usage : fibonacci(6) retourne [0, 1, 1, 2, 3, 5]
function fibonacci(nth = 2) { // on déclare une fonction qui s'appelle fibonacci et qui reçoit un paramètre nth (pour n ème) dont la valeur par défaut est 2.
  let fibo = [0, 1]; // créé un tableau qui contient les deux premiers membres de la suite de fibonacci.

  for (let index = 0; index < nth - 2; index++) { // tant que l'index est plus petit que le nombre de membres demandés
    fibo.push(fibo[index] + fibo[index + 1]); // on ajoute dans le tableau le membre suivant de la suite
  }

  return fibo // on retourne le tableau
}

let result = fibonacci();
console.log(result); // -> [0, 1]
console.log(fibonacci(5)); // -> [0, 1, 1, 2, 3]